angular.module('app')
	.factory
	( 'picasaService'
		, [ '$http' , '$q' , function( $http , $q ){

	$http.defaults.useXDomain = true;

	function parsePhoto(entry) {
		var lastThumb = entry.media$group.media$thumbnail.length - 1
		var photo = {
			thumb: entry.media$group.media$thumbnail[lastThumb].url,
			thumbHeight: entry.media$group.media$thumbnail[lastThumb].height,
			thumbWidth: entry.media$group.media$thumbnail[lastThumb].width,
			url: entry.media$group.media$content[0].url
		};
		return photo;
	}

	function parsePhotos(url) {
		var d = $q.defer();
		var photo;
		var photos = [];
		loadPhotos(url).then(function(data) {
			if (!data.feed) {
				photos.push(parsePhoto(data.entry));
			} else {
				var entries = data.feed.entry;
				for (var i = 0; i < entries.length; i++) {
					photos.push(parsePhoto(entries[i]));
				}
			}
			d.resolve(photos);

		});
		return d.promise;
	}

	function loadPhotos(url) {
		var d = $q.defer();
		$http.jsonp(url + '?alt=json&kind=photo&hl=pl&imgmax=912&callback=JSON_CALLBACK').success(function(data, status) {
			d.resolve(data);
		});
		return d.promise;
	}

	// Public API here
	return {
		get : function (url) {
				return parsePhotos(url);
			}
		}
	}]
)
