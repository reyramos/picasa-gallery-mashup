/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */


angular.module('app').controller(
	'ApplicationController'
	,[
		'$rootScope'
		, '$http'
		, '$scope'
		, '$resource'
		, 'picasaService'
		, '$timeout'
		, function
			(
				$rootScope
				, $http
				, $scope
				, $resource
				, picasaService
				, $timeout
				) {

			//http://picasaweb.google.com/data/feed/api/user/@userid?kind=photo&imgmax=912
			//http://picasaweb.google.com/data/feed/api/user/@userid/album/@album?imgmax=800

			$http.get('config/config.json').success(function(data) {
				console.log(data)

				$timeout(function(){
					load(data.url)
				}, 1000)

			});








			function init(slides){
				jQuery(function($){

					$.supersized({

						// Functionality
						slide_interval          :   3000,		// Length between transitions
						transition              :   3, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
						transition_speed		:	700,		// Speed of transition
						// Components
						slide_links				:	'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
						slides 	:  slides

					});
				});
			}


			function load (url){

				picasaService.get(url).then(function(data) {

					var slides  = []// Slideshow Images

					//http://buildinternet.com/project/supersized/docs.html#started_docs
					for(var i in data){
						var image = {image : data[i].url, thumb : data[i].thumb, url : data[i].url}

						slides.push(image)
					}



					init(slides)

				})

			}




		}]
)