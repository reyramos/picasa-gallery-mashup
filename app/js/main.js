// Require JS  Config File
require({
			baseUrl: 'js/',
			paths: {
				'ua-parser': '../lib/ua-parser-js/src/ua-parser'
				, 'angular': '../lib/angular/index'
				, 'angular-resource': '../lib/angular-resource/index'
				, 'angular-route': '../lib/angular-route/index'
				, 'jquery': '../lib/camera/scripts/jquery.min'
				, 'jquery-easing': 'jquery.easing.min'
				, 'supersized':'supersized.3.2.7'
				, 'supersized-shutter':'supersized.shutter'
			},
			shim: {
				'app': {
					'deps': [
						  'angular'
						, 'angular-route'
						, 'angular-resource'
						, 'supersized'
						, 'supersized-shutter'
					]
				},
				'angular-resource': { 'deps': ['angular','jquery'], 'exports':'angular' },
				'angular-route': { 'deps': ['angular'] },
				'supersized': { 'deps': ['jquery'] },
				'supersized-shutter': { 'deps': ['jquery'] },
				'jquery-easing': { 'deps': ['jquery'] },
				'routes': { 'deps': [
					'app'
				]},
				'ApplicationController': {
					'deps': [
						'app', 'picasaService'
					]},
				'picasaService': {
					'deps': [
						'app'
					]},
				'directives': {
					'deps': [
						'app'
					]}
			}
		},
		[
			'require'
			, 'routes'
			, 'ApplicationController'
			, 'picasaService'
			, 'directives'


		],
		function (require) {
			return require(
				[
					'bootstrap'
				]
			)
		}
);