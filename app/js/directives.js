angular.module('app').directive
	('onRepeatComplete', function () {
			var directive =
			{
				link: function (scope, element, attrs) {
					if (scope.$last){
						scope.$emit('ngRepeatComplete')
					}
				}
			};
			return directive;
		});