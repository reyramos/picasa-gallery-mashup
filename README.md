﻿#Angular Picasa Gellery

##What?
This is a gallery preview from a public folder in your google plus folder, change the setting to your picasa directory in the config folder to view your pictures

## Requirements ##
  * Node.js <http://nodejs.org/>
## Development Environment Setup ##
  1. Install Node.JS
	Use your system package manager (brew,port,apt-get,yum etc)
  2. Install global Bower, Grunt and PhantomJS commands, this step is not necessary,
  because once you perform grunt build it will install all the necessary files from package.json
	  ``bash
		sudo npm install -g grunt-cli bower phantomjs
	  ``
## Deployment ##
  1. Run build scripts
	  ``bash
	  grunt build
		``
  2. Serve compiled, production-ready files from the 'dist' directory
	  ``bash
	  grunt server:dist


##example
open the example folder to view my public pictures

##Working Progress
To autoload new images without the need of refresh