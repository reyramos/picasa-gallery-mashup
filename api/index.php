<?php


/**
 * FOR DEVELOPMENT ONLY:
 * 1. Make sure you have the mod_headers apache module installed
 * /etc/apache2/mods-enabled
 * If you dont have it install:
 * sudo ln -s /etc/apache2/mods-available/headers.load /etc/apache2/mods-enabled/headers.load
 *
 * 2. Add the Access-Control-Allow-Origin header to all HTTP response,
 * add the following to the desired <Directory>
 * Header set Access-Control-Allow-Origin "*"
 *
 * Within this file only add the headers "Access-Control-Allow-Headers: Content-Type"
 * remove this headers once you have it set onto your production server, and both client and server reside on the same machine
 */
header('Access-Control-Allow-Headers: Content-Type');


//some kind of return data
$data = array(
	"FROM" => "PHP RESTful:api"
, "status" => "OK");

/*
 * http://php.net/manual/en/function.http-get-request-body.php
 * Get the raw request body (e.g. POST or PUT data).
 * http_get_request_body needs to have PECL_HTTP module installed
 */
//$post_body = http_get_request_body ();


//get the data
$post_body = file_get_contents('php://input');
$jsonArray = json_decode($post_body);

$year = date('Y');
$month = date('m');
$day = date('d');

$dayMonth = date('t');
if ($day != $dayMonth) {
	$day++;
}

$day = sprintf("%02s", $day);

if ($month > 12) {
	$month = 1;
}

$month = sprintf("%02s", $month);


switch ($jsonArray->{'request_type'}) {

	case 'getCalendar':
		switch ($jsonArray->{'month'}) {
			case '-1':
				$result = array("$year-$month-$day", "$year-$month-" . sprintf("%02s", ($day - 1)));
				break;
			case 12:
				$result = array("$year-12-01", "$year-12-30");
				break;
			default:
				$data = array_merge($data, array("status" => "FALSE"));
				$result = array("ERROR" => "Failed Request");
				break;
		}
		break;
	default:
		$data = array_merge($data, array("status" => "FALSE"));
		$result = array("ERROR" => "Failed Request");
		break;

}


//Do what ever you want with the data
$data = array_merge($data, isset($result)? array("D" => $result):array());

$json = json_encode($data);
echo $json;